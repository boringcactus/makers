#![cfg(feature = "full")]

mod utils;

use std::fs;
use utils::{make, R};

#[test]
fn conditional_assignment_inheritance_test() -> R {
    let dir = tempfile::tempdir()?;

    let file_a = "
EGG = bug
include file_b.mk
check:
\t@echo $(EGG)
";
    fs::write(dir.path().join("Makefile"), file_a)?;
    let file_b = "
EGG ?= nope
";
    fs::write(dir.path().join("file_b.mk"), file_b)?;

    let result = make(&dir)?;
    assert!(result.status.success());
    assert_eq!(String::from_utf8(result.stdout)?.trim(), "bug");

    Ok(())
}
