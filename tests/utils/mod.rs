use std::path::Path;
use std::process::{Command, Output};

use eyre::Context;

pub type R = eyre::Result<()>;

pub fn make(dir: impl AsRef<Path>) -> eyre::Result<Output> {
    Command::new(env!("CARGO_BIN_EXE_makers"))
        .current_dir(dir)
        .output()
        .wrap_err("")
}
