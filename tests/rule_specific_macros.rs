#![cfg(feature = "full")]

mod utils;

use std::fs;
use utils::{make, R};

#[test]
fn target_specific_macros() -> R {
    let dir = tempfile::tempdir()?;

    let file = "
foo.h: EGG = bug
foo.h:
\techo $(EGG)
    ";
    fs::write(dir.path().join("Makefile"), file)?;

    let result = make(&dir)?;
    dbg!(&result);
    assert!(result.status.success());
    let stdout = String::from_utf8(result.stdout)?;
    assert!(stdout.contains("echo bug"));

    Ok(())
}

#[test]
#[ignore = "not yet implemented"]
fn target_specific_macros_inherited() -> R {
    let dir = tempfile::tempdir()?;

    // example from https://www.gnu.org/software/make/manual/html_node/Target_002dspecific.html
    let file = "
CC=echo cc
prog : CFLAGS = -g
prog : prog.o foo.o bar.o
    ";
    fs::write(dir.path().join("Makefile"), file)?;
    fs::write(dir.path().join("prog.c"), "")?;
    fs::write(dir.path().join("foo.c"), "")?;
    fs::write(dir.path().join("bar.c"), "")?;

    let result = make(&dir)?;
    dbg!(&result);
    assert!(result.status.success());
    let stdout = String::from_utf8(result.stdout)?;
    assert!(stdout.contains("echo cc -g -c foo.c"));
    assert!(stdout.contains("echo cc -g -c bar.o"));
    assert!(stdout.contains("echo cc -g -c prog.c"));

    Ok(())
}

#[test]
#[ignore = "not yet implemented"]
fn inference_rule_specific_macros() -> R {
    let dir = tempfile::tempdir()?;

    // example from https://www.gnu.org/software/make/manual/html_node/Pattern_002dspecific.html
    let file = "
CC=echo cc
%.o: %.c
\t$(CC) -c $(CFLAGS) $(CPPFLAGS) $< -o $@

lib/%.o: CFLAGS := -fPIC -g
%.o: CFLAGS := -g

all: foo.o lib/bar.o
    ";
    fs::write(dir.path().join("Makefile"), file)?;
    fs::write(dir.path().join("foo.c"), "")?;
    fs::create_dir(dir.path().join("lib"))?;
    fs::write(dir.path().join("bar.c"), "")?;

    let result = make(&dir)?;
    dbg!(&result);
    assert!(result.status.success());
    let stdout = String::from_utf8(result.stdout)?;
    dbg!(&stdout);
    assert!(stdout.contains("echo cc -g foo.c -o foo.o"));
    assert!(stdout.contains("echo cc -fPIC -g lib/bar.c -o lib/bar.o"));

    Ok(())
}
