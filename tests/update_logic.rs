use std::fs;
use std::thread::sleep;
use std::time::Duration;

mod utils;
use utils::{make, R};

#[test]
fn basic_test() -> R {
    let dir = tempfile::tempdir()?;
    let file = "
based-on-what: based-on-this
\techo hi
    ";
    fs::write(dir.path().join("Makefile"), file)?;
    let result = make(&dir)?;
    assert!(!result.status.success());
    assert!(String::from_utf8(result.stderr)?.contains("based-on-this"));

    fs::write(dir.path().join("based-on-this"), "")?;
    let result = make(&dir)?;
    assert!(result.status.success());
    assert!(String::from_utf8(result.stdout)?.contains("echo hi"));

    let result = make(&dir)?;
    assert!(result.status.success());
    assert!(String::from_utf8(result.stdout)?.contains("echo hi"));

    fs::write(dir.path().join("based-on-what"), "")?;
    let result = make(&dir)?;
    assert!(result.status.success());
    assert!(!String::from_utf8(result.stdout)?.contains("echo hi"));

    sleep(Duration::from_millis(1000));

    fs::write(dir.path().join("based-on-this"), "")?;
    let result = make(&dir)?;
    assert!(result.status.success());
    assert!(String::from_utf8(result.stdout)?.contains("echo hi"));

    Ok(())
}
