# makers

A (mostly) [POSIX-compatible](https://pubs.opengroup.org/onlinepubs/9699919799/utilities/make.html) make implemented in Rust.
Not explicitly aiming for full support for [every GNU make feature](https://www.gnu.org/software/make/manual/html_node/index.html), but adding whichever features are strictly necessary to be compatible with existing GNUish makefiles.

You can get slightly more info by running with `RUST_LOG=makers=warn`, much more info with `RUST_LOG=makers=debug`, and an avalanche of info (probably only useful in small doses for debugging your makefile or makers itself) with `RUST_LOG=makers=trace`.
For more specific configuration, see [the documentation for the `env_logger` Rust crate](https://docs.rs/env_logger/0.11.5/env_logger/).

## conformance

- internationalization (`LANG`/`LC_ALL`/`LC_CTYPE`/`LC_MESSAGES`) not implemented
- XSI conformance (SCCS integration) not implemented
- signal handling not implemented
- library handling not implemented
- some POSIX-specified features are pending (search `TODO` for a list)
- builtin macro `MAKE` is defined as the path to `makers`
- specifying the `.POSIX` special target doesn't opt out of extensions (but you can install with `--no-default-features` to get only POSIX behavior)

## status

i haven't written unit tests for most of `makers`, but there are [integration tests](https://gitlab.com/boringcactus/makers/-/pipelines) that try to build various make-based projects with makers.

specific projects tested:

- [tcl](https://tcl.tk/)
- [uutils coreutils](https://github.com/uutils/coreutils)
- [CPython](https://github.com/python/cpython)
- [SPDX license list](https://github.com/spdx/license-list-XML)
- [libarchive](https://libarchive.org/)
- [xbps](https://github.com/void-linux/xbps)

list of features that are needed for version 1.0:

- actual documentation
- tested with Linux
- unit tests for most of the things
- no TODOs left

## changelog

### next - TBD

- propagate command line macros through `MAKEFLAGS` for recursive calls

### v0.8.0 - 2024-11-15

- license is worse
- implement `-C`/`--directory` flag
- pass flags into recursive calls via `MAKEFLAGS` environment variable
- implement GNU-style `export EGG=bug`
- rebuild out-of-date makefiles (but not with as much environment preservation as GNU make provides)
- implement GNU-style rule-specific macros `target: EGG=bug`
- implement GNU-style `.SECONDEXPANSION` (but not properly deferred like in GNU make)

### v0.7.0 - 2021-04-13

- implement GNU-style functions `abspath`, `firstword`
- implement GNU-style scoped inference rules ("static patterns")

### v0.6.1 - 2021-04-06

- make `export`/`unexport` actually persist so they're available while executing

### v0.6.0 - 2021-04-06

- implement GNU-style functions `findstring`, `realpath`, `dir`, `lastword`, `words`, `subst`, `error`, `word`, `patsubst`, `addsuffix`
- provide GNU-style built-in variables `MAKECMDGOALS`, `CURDIR`, `MAKEFILE_LIST`
- fix broken handling of nested conditionals
- try to build a missing included makefile before hard-erroring
- implement GNU-style `export`/`unexport` directives

### v0.5.0 - 2021-04-04

- add verbose logging
- partially fix `$<` (old: all dependencies, new: first dependency, spec: first dependency added by inference rule)
- set the `$(MAKE)` built-in macro to the full path to `makers` if it can be found
- change `%`-matching to reject partial pattern matches but allow all characters in `%`
- implement GNU-style functions `and`, `or`, `strip`

### v0.4.0 - 2021-04-03

- fix newline handling so escaped newlines & conditional lines work the way they're supposed to
- implement GNU-style `%`-matching inference rules
- dependencies of `.PHONY` special target are now always assumed out-of-date until rebuilt, regardless of filesystem status
- give user-defined inference rules priority over built-in ones

### v0.3.0 - 2021-04-03

- allow installing with `--no-default-features` for pure-POSIX compatibility
- implement GNU-style functions `if`, `origin`, `shell` (doesn't set `.SHELLSTATUS`), `eval`
- implement GNU-style `define` blocks
- improve error reporting

### v0.2.0 - 2021-04-01

- partially implement GNU-style conditionals (only parentheses around arguments are supported)
- implement some GNU-style functions: `filter`, `filter-out`, `sort`, `notdir`, `basename`, `addprefix`, `wildcard`, `foreach`, `call`
- improve error reporting for parse errors

### v0.1.0 - 2021-03-25

- initial release

## contributors

-   [zseri](https://ytrizja.de/) (code)
-   [@Lucretiel](https://github.com/Lucretiel) (code)
-   you, if you'd like!
    email melody@boringcactus.com if you've got something ([not just code!](https://allcontributors.org/docs/en/emoji-key)) to contribute

## license

Released under [Big Bill Hell’s License](https://lifning.info/BBHL).

Exceptions may be available on request.

## minuteæ

Canonically pronounced /ˈmeɪ.kɹɪz/ (as though saying "make risotto" but giving up halfway through).
