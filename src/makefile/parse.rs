use super::Macro;
use super::TokenString;

#[derive(Debug)]
pub struct MacroAssignment {
    pub name: String,
    pub value: TokenString,
    #[cfg(feature = "full")]
    pub expand_value: bool,
    #[cfg(feature = "full")]
    pub skip_if_defined: bool,
    #[cfg(feature = "full")]
    pub append: bool,
}

pub enum MacroAssignmentOutcome {
    Set,
    AppendedTo(Macro),
}
